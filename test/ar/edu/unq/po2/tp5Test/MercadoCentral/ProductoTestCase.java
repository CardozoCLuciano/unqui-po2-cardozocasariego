package ar.edu.unq.po2.tp5Test.MercadoCentral;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp5.MercadoCentral.Producto;

class ProductoTestCase {
	
	Producto leche;
	
	@BeforeEach
	public void setUp() {
		
		leche = new Producto(50d, 10);		
	}

	@Test
	void unProductoTieneUnPrecioYunaCantidadDeStock() {
		
		assertEquals(50d , leche.getPrecio());
		assertEquals(10, leche.cantStock());
	}
	
	@Test
	void unProductoreduceSuCantidadDeStock() {
		
		leche.descontarDelStock(1);
		
		assertEquals(9, leche.cantStock());
	}

}
