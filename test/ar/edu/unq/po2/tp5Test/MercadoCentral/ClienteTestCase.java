package ar.edu.unq.po2.tp5Test.MercadoCentral;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import ar.edu.unq.po2.tp5.MercadoCentral.Cliente;
import ar.edu.unq.po2.tp5.MercadoCentral.Producto;
import ar.edu.unq.po2.tp5.MercadoCentral.ProductoCoop;

class ClienteTestCase {
	
	Cliente cliente01;
	
	Producto leche;
	ProductoCoop arroz;
	
	
	@BeforeEach
	public void setUp() {
		
		cliente01 = new Cliente();
		leche = new Producto(100d, 2);
		arroz = new ProductoCoop(100d, 2);
		
	}

	@Test
	void unClienteAgregaProductosASuCarrito() {
		
		assertEquals(0, cliente01.getCarrito().size());
		cliente01.agregarAlCarro(leche);
		assertEquals(1, cliente01.getCarrito().size());
		cliente01.agregarAlCarro(arroz);
		assertEquals(2, cliente01.getCarrito().size());
	}

}
