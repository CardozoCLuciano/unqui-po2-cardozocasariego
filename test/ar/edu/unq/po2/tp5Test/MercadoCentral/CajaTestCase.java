package ar.edu.unq.po2.tp5Test.MercadoCentral;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp5.MercadoCentral.Caja;
import ar.edu.unq.po2.tp5.MercadoCentral.Cliente;
import ar.edu.unq.po2.tp5.MercadoCentral.Producto;
import ar.edu.unq.po2.tp5.MercadoCentral.ProductoCoop;

class CajaTestCase {
	
	Caja caja01;
	
	Cliente cliente01;
	
	Producto leche;
	Producto arroz;
	ProductoCoop azucar;

	@BeforeEach
	public void setUp() {
		
		caja01 = new Caja();
		
		cliente01 = new Cliente();
		
		leche = new Producto(50d, 2);
		arroz = new Producto(80d, 5);
		azucar = new ProductoCoop(100d, 1);
	}
	
	@Test
	void unaCajaSabeElCostoDelCarritoDelCliente() {
		
		double actual = caja01.costoDelCarritoDe(cliente01);
		assertEquals(0 , actual);
		
		cliente01.agregarAlCarro(arroz);
		cliente01.agregarAlCarro(azucar);
		cliente01.agregarAlCarro(leche);
		
		 actual = caja01.costoDelCarritoDe(cliente01);
		 assertEquals(220d , actual);
	}
	
	@Test
	void unaCajaRegistraElCarritoDelCliente() {
		
		cliente01.agregarAlCarro(arroz);
		cliente01.agregarAlCarro(azucar);
		cliente01.agregarAlCarro(leche);
		
		caja01.registrarProductosDe(cliente01);
		
		assertEquals(4 , arroz.cantStock());
		assertEquals(1 , leche.cantStock());
		assertEquals(0 , azucar.cantStock());
		
		assertEquals(220d , cliente01.getMontoAPagar() );
		 
	}

}
