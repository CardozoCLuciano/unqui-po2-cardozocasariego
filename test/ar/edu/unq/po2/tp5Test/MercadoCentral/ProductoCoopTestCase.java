package ar.edu.unq.po2.tp5Test.MercadoCentral;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp5.MercadoCentral.ProductoCoop;

class ProductoCoopTestCase {
	
	ProductoCoop arroz;
	
	@BeforeEach
	public void setUp() {
		
		arroz = new ProductoCoop(100d, 2);
	}

	@Test
	void unProductoCoopTiene10PorcientoDeDescuento() {
		
		assertEquals(90, arroz.getPrecio());
	}

}
