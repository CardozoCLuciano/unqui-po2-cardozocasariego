package ar.edu.unq.po2.tp4Test.Trabajador;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp4.Trabajador.Ingreso;
import ar.edu.unq.po2.tp4.Trabajador.IngresoHorasExtras;
import ar.edu.unq.po2.tp4.Trabajador.Trabajador;

import static org.junit.jupiter.api.Assertions.*;

class TrabajadorTestCase {
	
	Trabajador trabajadorA;	
	
	Ingreso ingresoEnero;
	Ingreso ingresoFebrero;
	
	IngresoHorasExtras ingresoEneroHorasExt;
	
	@BeforeEach
	public void setUp(){
		
		trabajadorA = new Trabajador();	
		
		ingresoEnero = new Ingreso("Enero", "Primer ingreso" , 10000d);
		ingresoFebrero = new Ingreso("Febrero", "Segundo ingreso" , 10000d);
		
		ingresoEneroHorasExt = new IngresoHorasExtras("Febrero", "Segundo ingreso" , 300d, 5);
	}

	@Test
	void unTrabajadorComienzaSinIngresos() {
		
		assertEquals(0d, trabajadorA.getTotalPercibido());					
	}
	
	@Test
	void unTrabajadorObtiene2IngresosDe10000() {
		
		trabajadorA.añadirIngreso(ingresoEnero);
		trabajadorA.añadirIngreso(ingresoFebrero);
		
		assertEquals(20000d, trabajadorA.getTotalPercibido());					
	}
	
	@Test
	void unTrabajadorObtiene1IngresoPorHorasExtrasDe300() {
		
		trabajadorA.añadirIngreso(ingresoEneroHorasExt);		
		
		assertEquals(300d, trabajadorA.getTotalPercibido());					
	}
	
	
	@Test
	void unTrabajadorNoTieneMontoImponiblesPorHorasExtras() {
		
		trabajadorA.añadirIngreso(ingresoEneroHorasExt);
		
		assertEquals(300d, trabajadorA.getTotalPercibido()); // Para confirmar que se registro el ingreso
		assertEquals(0d, trabajadorA.getMontoImponible());					
	}
	
	
	@Test
	void UnTrabajadorTieneVariosIngresosDeAmbosTipos() {
		
		trabajadorA.añadirIngreso(ingresoEneroHorasExt);
		
		trabajadorA.añadirIngreso(ingresoEnero);
		trabajadorA.añadirIngreso(ingresoFebrero);
		
		assertEquals(20300d, trabajadorA.getTotalPercibido()); 
		assertEquals(20000d, trabajadorA.getMontoImponible());					
	}
	
	
	@Test
	void unTrabajadorSabeCuantoTieneQuePagarDeImpuestosSinIngresos() {
		
		assertEquals(0d, trabajadorA.getImpuestoAPagar());					
	}
	
	@Test
	void unTrabajadorSabeCuantoTieneQuePagarDeImpuestosConUnIngreso() {
		
		trabajadorA.añadirIngreso(ingresoEnero);
		
		assertEquals(200d, trabajadorA.getImpuestoAPagar());					
	}
	
	@Test
	void unTrabajadorSabeCuantoTieneQuePagarDeImpuestosConVariosIngresos() {
		
		trabajadorA.añadirIngreso(ingresoEneroHorasExt);
		
		trabajadorA.añadirIngreso(ingresoEnero);
		trabajadorA.añadirIngreso(ingresoFebrero);
		
		assertEquals(20300d, trabajadorA.getTotalPercibido()); // Para confirmar que se registraron los ingresos
		assertEquals(20000d, trabajadorA.getMontoImponible()); // Para confirmar que se registraron los ingresos
		assertEquals(400d, trabajadorA.getImpuestoAPagar());					
	}


}
