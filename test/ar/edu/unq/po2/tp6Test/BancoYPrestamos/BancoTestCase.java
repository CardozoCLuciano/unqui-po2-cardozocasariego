package ar.edu.unq.po2.tp6Test.BancoYPrestamos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp6.BancoYPrestamos.Banco;
import ar.edu.unq.po2.tp6.BancoYPrestamos.Cliente;
import ar.edu.unq.po2.tp6.BancoYPrestamos.Propiedad;

class BancoTestCase {
	
	public Banco unBanco;
	public Cliente unCliente;
	public Propiedad unaPropiedad;
	public Cliente cliente2;
	public Cliente cliente3;
	
	@BeforeEach
	public void setUp() {
		
		unBanco = new Banco();
		unCliente = new Cliente("Juan", "Pereira", 15, "Calle falsa" , 700);
		unaPropiedad = new Propiedad("Calle super falsa", "Alta Propiedad" , 300000);
		cliente2 = new Cliente("Luis", "Pereira", 15, "Calle falsa" , 10700);
		cliente3 = new Cliente("Luis", "Pereira", 15, "Calle falsa" , 10700);
	}

	@Test
	void unBancoPuedeAgregarUnNuevoCliente() {
		
		assertEquals(0 , unBanco.getClientesList().size());
		unBanco.agregarAlCliente(unCliente);
		assertEquals(1 , unBanco.getClientesList().size());
	}

	@Test
	void unClienteLePuedePedirAlBancoUnCreditoPersonal() {
		
		assertEquals(0 , unBanco.getCreditosMap().size());
		unCliente.SolicitarCreditoPersonal(10000, 10, unBanco);
		assertEquals(1 , unBanco.getCreditosMap().size());
	}
	
	@Test
	void unClienteLePuedePedirAlBancoUnCreditoHipotecario() {
		
		assertEquals(0 , unBanco.getCreditosMap().size());
		unCliente.SolicitarCreditoHipotecario(10000, 10, unBanco, unaPropiedad);
		assertEquals(1 , unBanco.getCreditosMap().size());
	}
	
	
	@Test
	void ElBancoPuedeCheckearUnCreditoPersonal() {
		
		unCliente.SolicitarCreditoPersonal(10000, 10, unBanco);
		assertEquals(1 , unBanco.getCreditosMap().size());
		
		
		unBanco.checkarCredito(unCliente);
		assertEquals(0 , unBanco.getCreditosMap().size());
	}
	
	
	@Test
	void ElBancoPuedeCheckearUnCreditoHipotecario() {
		
		unCliente.SolicitarCreditoHipotecario(10000, 10, unBanco, unaPropiedad);
		assertEquals(1 , unBanco.getCreditosMap().size());
		
		
		unBanco.checkarCredito(unCliente);
		assertEquals(0 , unBanco.getCreditosMap().size());
	}
	
	@Test
	void unBancoSabeElTotalDeCuantoTieneQuePagarPorLosCreditosAceptados() {
		
		cliente2.SolicitarCreditoHipotecario(500, 10, unBanco, unaPropiedad);		
		cliente3.SolicitarCreditoHipotecario(1500, 10, unBanco, unaPropiedad);	
		
		assertEquals(2000 , unBanco.calcularTotalAPagarPorCreditos());
	}
	
	
	
	
	
	
	
	
	
	
	
}
