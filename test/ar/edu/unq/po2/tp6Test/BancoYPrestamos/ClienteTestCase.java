package ar.edu.unq.po2.tp6Test.BancoYPrestamos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp6.BancoYPrestamos.Cliente;

class ClienteTestCase {
	
	public Cliente unCliente;
	
	@BeforeEach
	public void setp() {
		
		unCliente = new Cliente("Pepe", "Pereira", 23, "Calle falsa" , 25000);
	}

	
	@Test
	void constructorDeCliente() {
		
		assertEquals("Pepe", unCliente.getNombre());
		assertEquals("Pereira", unCliente.getApellido());
		assertEquals(23, unCliente.getEdad());
		assertEquals("Calle falsa", unCliente.getDireccion());
		assertEquals(25000, unCliente.getSueldoMensual());
		assertEquals(300000, unCliente.getSueldoAnual());
	}
	
	

}
