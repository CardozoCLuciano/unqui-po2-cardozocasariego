package ar.edu.unq.po2.tp6Test.BancoYPrestamos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp6.BancoYPrestamos.Cliente;
import ar.edu.unq.po2.tp6.BancoYPrestamos.CreditoHipotecario;
import ar.edu.unq.po2.tp6.BancoYPrestamos.CreditoPersonal;
import ar.edu.unq.po2.tp6.BancoYPrestamos.Propiedad;

class CreditoTestCase {
	
	public CreditoPersonal unCreditoPersonal;
	public CreditoHipotecario unCreditoHipotecario;
	
	
	public Cliente unaPersonaApta;
	public Cliente unaPersonaNoApta;
	
	public Propiedad unaPropiedad;
	
	@BeforeEach
	public void setUp() {
		
		unaPropiedad = new Propiedad("Calle super falsa", "Alta Propiedad" , 300000);
		
		unCreditoPersonal = new CreditoPersonal(10000, 5);
		unCreditoHipotecario = new CreditoHipotecario(10000, 5, unaPropiedad);
		
		
		unaPersonaApta = new Cliente("Pepe", "Pereira", 30, "Calle falsa" , 25000);
		unaPersonaNoApta = new Cliente("Juan", "Pereira", 15, "Calle falsa" , 700);
	}
	
	@Test
	void ConstructorDeCreditos() {
		
		assertEquals(10000, unCreditoPersonal.getMonto());
		assertEquals(5, unCreditoPersonal.getCantCuotas());
	}
	
	@Test
	void unCreditoPersonalEvaluaSiEsPosible() {
		
		assertTrue(unCreditoPersonal.evaluarCredito(unaPersonaApta));
		assertFalse(unCreditoPersonal.evaluarCredito(unaPersonaNoApta));		
	}
	
	@Test
	void unCreditoHipotecarioEvaluaSiEsPosible() {
		
		assertTrue(unCreditoHipotecario.evaluarCredito(unaPersonaApta));
		assertFalse(unCreditoHipotecario.evaluarCredito(unaPersonaNoApta));		
	}

}
