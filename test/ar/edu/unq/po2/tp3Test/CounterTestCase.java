package ar.edu.unq.po2.tp3Test;


import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ar.edu.unq.po2.tp3.Counter;

class CounterTestCase {
	
	private Counter counter;
	
	@BeforeEach
	public void setUp() throws Exception{
		counter = new Counter();
		
		counter.addNumber(9);
		counter.addNumber(7);
		counter.addNumber(5);
		counter.addNumber(3);
		counter.addNumber(19);
		counter.addNumber(17);
		counter.addNumber(15);
		counter.addNumber(13);
		counter.addNumber(11);		
		counter.addNumber(4);
	}

	@Test
	void testElCounterSabeQueHayUnSoloNumeroPar() {
		
		int actual = counter.cantidadDePares();
		
		int esperado = 1;
		
		assertEquals(esperado, actual);		
	}
	
	
	@Test
	void testElCounterSabeQueHay9NumerosImpares() {
		
		int actual = counter.cantidadDeImares();
		
		int esperado = 9;
		
		assertEquals(esperado, actual);		
	}
	
	
	@Test
	void testElCounterSabeQueCuantosSonMultiplosDe3() {
		
		int actual = counter.multiplosDe(3);
		
		int esperado = 3;
		
		assertEquals(esperado, actual);		
	}
	
	

}
