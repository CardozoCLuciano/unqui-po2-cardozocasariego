package ar.edu.unq.po2.tp3Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp3.MultiOperador;

class MultiOperadorTestCase {
	
	MultiOperador operador;
	
	@BeforeEach
	public void setUp()	{
		
		operador = new MultiOperador();
		
		operador.addNumber(1);
		operador.addNumber(2);
		operador.addNumber(3);
		operador.addNumber(4);
		operador.addNumber(5);
	}
	
	@Test
	 void laSumaDeTodaLaListaEs15() {
		
		int actual = operador.sumarLaLista();
		int esperado = 15;
		
		assertEquals(esperado, actual);
		
	}

	@Test
	 void laRestaDeTodaLaListaEsMenos12() {
		
		int actual = operador.restarLaLista();
		int esperado = (-13);
		
		assertEquals(esperado, actual);
	}
	
	@Test
	 void laMultiplicacionDeTodaLaListaEs() {
		
		int actual = operador.multiplicarLaLista();
		int esperado = 120;
		
		assertEquals(esperado, actual);
	}
	
}












