package ar.edu.unq.po2.tp3Test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import ar.edu.unq.po2.tp3.Point;


class PointTestCase {
	
	Point puntoA;
	Point puntoB;
	

	@Test
	void unPuntoSinParametrosSeUbicaEnEl00() {
		
		puntoA = new Point();
		
		boolean esperado = (puntoA.getCoordenadasX() == 0) && (puntoA.getCoordenadasY() == 0);		
		
		Assert.assertTrue(esperado);
	}
	
	@Test
	void unPuntoTieneLasCoordenadasQueSeLePasanPorParametro() {
		
		puntoA = new Point(6,5);
		
		boolean esperado = (puntoA.getCoordenadasX() == 6) && (puntoA.getCoordenadasY() == 5);		
		
		Assert.assertTrue(esperado);
	}
	
	
	@Test
	void unPuntoPuedeModificarSusCoordenadas() {
		
		puntoA = new Point();
		puntoA.moverPuntoA(5,2);
		
		
		boolean esperado = (puntoA.getCoordenadasX() == 5) && (puntoA.getCoordenadasY() == 2);		
		
		Assert.assertTrue(esperado);
	}
	
	
	@Test
	void dosPuntosPuedenSumarSusCoordenadas() {
		
		puntoA = new Point(5,5);
		puntoB = new Point(3,2);
		
		puntoA.sumarCoordCon(puntoB);
		
		boolean esperado = (puntoA.getCoordenadasX() == 8) && (puntoA.getCoordenadasY() == 7);		
		
		Assert.assertTrue(esperado);
	}

}
