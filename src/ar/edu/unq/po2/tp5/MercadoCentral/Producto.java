package ar.edu.unq.po2.tp5.MercadoCentral;

public class Producto {
	
	//Atributos
	protected double precio;
	private int stock;

	//Costructor
	public Producto(double unPrecio, int unaCantDeStock) {
		
		this.precio = unPrecio;
		this.stock = unaCantDeStock;
	}

	//Getters
	public double getPrecio() {
		
		return this.precio;
	}

	//Metodos
	public int cantStock() {
		
		return this.stock;
	}

	public void descontarDelStock(int unaCantidad) {
		
		this.stock -= unaCantidad;
	}

}
