package ar.edu.unq.po2.tp5.MercadoCentral;

public class Caja {
	
	//Metodos
	public double costoDelCarritoDe(Cliente unCliente) {
		
		double total = 0;
		
		for(Producto elem:unCliente.getCarrito()) {
			
			total += elem.getPrecio();
		}
		return total;
	}

	public void registrarProductosDe(Cliente unCliente) {
		
		for(Producto elem:unCliente.getCarrito()) {			
			
			elem.descontarDelStock(1);
		}
		this.informarClienteMontoApagar(unCliente);
	}

	private void informarClienteMontoApagar(Cliente unCliente) {
		
		double montoAPagar = this.costoDelCarritoDe(unCliente);
		
		unCliente.setMontoAPagar(montoAPagar) ;
	}

}
