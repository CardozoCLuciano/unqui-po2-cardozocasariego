package ar.edu.unq.po2.tp5.MercadoCentral;

public class ProductoCoop extends Producto{
	
	//Costructor
	public ProductoCoop(double precio, int stock) {		
		
		super(precio , stock);
	}

	
	//Getters
	public double getPrecio() {
		
		return (this.precio * 0.9);
	}
}
