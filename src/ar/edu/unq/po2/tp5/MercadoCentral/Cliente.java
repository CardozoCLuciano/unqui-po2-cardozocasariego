package ar.edu.unq.po2.tp5.MercadoCentral;

import java.util.ArrayList;

public class Cliente {
	
	//Atributos
	private ArrayList<Producto> carrito;
	private double montoApagar;
	
	
	//Costructor
	public Cliente() {
		
		this.carrito = new ArrayList<Producto>();
		this.montoApagar = 0;
	}
	
	//Getters
	public ArrayList<Producto> getCarrito() {
		
		return this.carrito;
	}

	//Setters
	public void setMontoAPagar(double montoAPagar) {
		
		this.montoApagar = montoAPagar;
	}
	
	//Metodos
	public void agregarAlCarro(Producto unProducto) {
	
		this.carrito.add(unProducto);
	}

	
	public Double getMontoAPagar() {
		
		return this.montoApagar;
	}

}
