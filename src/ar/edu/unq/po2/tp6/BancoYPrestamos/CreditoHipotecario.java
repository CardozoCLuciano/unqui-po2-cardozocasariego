package ar.edu.unq.po2.tp6.BancoYPrestamos;

public class CreditoHipotecario extends Credito{
	
	private Propiedad propiedadGarantia;

	//Constructor
	public CreditoHipotecario(float unMonto, int unaCantCuotas, Propiedad unaPropiedad) {
		super(unMonto, unaCantCuotas);
		this.propiedadGarantia = unaPropiedad;
		
	}	
	
	
	//Metodos
	public boolean evaluarCredito(Cliente unaPersona) {
		
		boolean esApto = false;
		
		if(
			((this.getMonto() / this.getCantCuotas()) < (unaPersona.getSueldoMensual() * 0.5))  &&
			(this.getMonto() < (this.propiedadGarantia.getValorFiscal() * 0.7)) &&
			( unaPersona.getEdad() + (this.getCantCuotas() / 12)  < 65)		
		){
			
			esApto = true;
		}
		
		return esApto;
	}

	

}
