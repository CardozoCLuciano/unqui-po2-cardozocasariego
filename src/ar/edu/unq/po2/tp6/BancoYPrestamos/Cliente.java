package ar.edu.unq.po2.tp6.BancoYPrestamos;

public class Cliente {
	
	//Atributos
	
	private String nombre;
	private String apellido;
	private int edad;
	private String direccion;
	private float sueldoMensual;
	private float sueldoAnual;
	
	//Constructor

	public Cliente(String unNombre, String unApellido, int edad, String unaDireccion, float sueldoMensual) {
		
		this.apellido = unApellido;
		this.direccion = unaDireccion;
		this.edad = edad;
		this.nombre = unNombre;
		this.sueldoMensual = sueldoMensual;
		this.sueldoAnual = sueldoMensual * 12;
	}
	
	// Getters

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public int getEdad() {
		return edad;
	}

	public String getDireccion() {
		return direccion;
	}

	public float getSueldoMensual() {
		return sueldoMensual;
	}

	public float getSueldoAnual() {
		return sueldoAnual;
	}
	
	//Metodos

	public void SolicitarCreditoPersonal(int unMonto, int unaCantDeCuotas, Banco unBanco) {
		
		CreditoPersonal nuevoCredito = new CreditoPersonal(unMonto, unaCantDeCuotas);
		
		unBanco.agregarElCredito(nuevoCredito, this);
		
	}

	public void SolicitarCreditoHipotecario(int unMonto, int unaCantDeCuotas, Banco unBanco, Propiedad unaPropiedad) {
	
		CreditoHipotecario nuevoCredito = new CreditoHipotecario(unMonto, unaCantDeCuotas, unaPropiedad);
		
		unBanco.agregarElCredito(nuevoCredito, this);		
	}
	
	
	

}
