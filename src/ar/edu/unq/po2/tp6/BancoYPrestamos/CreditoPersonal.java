package ar.edu.unq.po2.tp6.BancoYPrestamos;

public class CreditoPersonal extends Credito{

	//Constructor
	public CreditoPersonal(float unMonto, int unaCantCuotas) {
		super(unMonto, unaCantCuotas);
		
	}

	
	//Metodos
	public boolean evaluarCredito(Cliente unaPersona) {
		
		boolean esApto = false;
		
		if( (unaPersona.getSueldoAnual() > 15000) && ( (this.getMonto() / this.getCantCuotas() ) < (unaPersona.getSueldoMensual() * 0.7) ) ) {
			
			esApto = true;
		}
		
		return esApto;
	}

	
}
