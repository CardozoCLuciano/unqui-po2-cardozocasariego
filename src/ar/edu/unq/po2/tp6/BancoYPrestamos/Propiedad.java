package ar.edu.unq.po2.tp6.BancoYPrestamos;

public class Propiedad {
	
	private String direccion;
	private String descripcion;
	private float valorFiscal;
	
	
	//Canstructor
	public Propiedad(String direccion, String descripcion, float valorFiscal) {
		
		this.descripcion = descripcion;
		this.direccion = direccion;
		this.valorFiscal = valorFiscal;
	}
	
	//Getters

	public String getDireccion() {
		return direccion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public float getValorFiscal() {
		return valorFiscal;
	}

}
