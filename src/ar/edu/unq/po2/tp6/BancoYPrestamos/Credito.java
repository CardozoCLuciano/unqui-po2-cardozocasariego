package ar.edu.unq.po2.tp6.BancoYPrestamos;

public abstract class Credito {
	
	//Metodos
	private float monto;
	private int cantCuotas;
	
	//Constructor
	public Credito(float unMonto, int unaCantCuotas) {
	
		this.cantCuotas = unaCantCuotas;
		this.monto = unMonto;
	}
	
	// Getters
	public float getMonto() {
		return monto;
	}

	public int getCantCuotas() {
		return cantCuotas;
	}
	
	//Metodos
	
	public abstract boolean evaluarCredito(Cliente unaPersona);

}
