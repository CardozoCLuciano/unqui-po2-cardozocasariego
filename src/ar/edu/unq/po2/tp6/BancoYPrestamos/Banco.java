package ar.edu.unq.po2.tp6.BancoYPrestamos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Banco {
	
	//Atributos
	private ArrayList<Cliente> clientesList;
	private Map<Cliente, Credito> mapDeCreditos;
	
	
	//Constructor
	
	public Banco() {
		
		this.clientesList = new ArrayList<Cliente>();
		this.mapDeCreditos = new HashMap<Cliente, Credito>();
	}
	
	//Getters
	public ArrayList<Cliente> getClientesList() {
		
		return clientesList;
	}
	
	public Map<Cliente, Credito> getCreditosMap() {
		
		return mapDeCreditos;
	}
	
	//Metodos

	public void agregarAlCliente(Cliente unCliente) {
		
		this.clientesList.add(unCliente);		
	}

	
	public void agregarElCredito(Credito nuevoCredito, Cliente unCliente) {		
		
		mapDeCreditos.put(unCliente, nuevoCredito);
	}

	
	
	public void checkarCredito(Cliente unCliente) {
		
		Credito unCredito = mapDeCreditos.get(unCliente);
		
		if( unCredito.evaluarCredito(unCliente)) {
			
			this.aceptarCredito(unCliente);
		}else {
			
			this.rechazarCredito(unCliente);
		}		
	}
	

	private void rechazarCredito(Cliente unCliente) {
		
		mapDeCreditos.remove(unCliente);
	}

	
	private void aceptarCredito(Cliente unCliente) {
		
		//Hace lo que tenga que hacer al aceptar y luego:
		
		mapDeCreditos.remove(unCliente);
	}

	public float calcularTotalAPagarPorCreditos() {
		
		float totalAPagar = 0;
		
		
		
		for (Map.Entry<Cliente, Credito> elem : mapDeCreditos.entrySet()) {
		    
			Credito unCredito = mapDeCreditos.get(elem.getKey());
			
			if( unCredito.evaluarCredito(elem.getKey())) {
				
				totalAPagar += unCredito.getMonto();
			}			
		}
		return totalAPagar;
	}

	

}
