package ar.edu.unq.po2.tp4.Supermercado;

public class ProductoPrimeraNecesidad extends Producto{	
	
	private double descuento;
	

	public ProductoPrimeraNecesidad(String unNombre, double unPrecio, boolean esCuidado, double unDescuento){
		
		super(unNombre, unPrecio, esCuidado);
		this.descuento = unDescuento;
	}
	
	
	public double getPrecio() {
		
		double precioFinal = this.precio - (this.precio * (this.descuento / 100));
		
		return precioFinal;
	}

}
