package ar.edu.unq.po2.tp4.Supermercado;

public class Producto {
	
	//Variables de instancia
	private String nombre;
	protected double precio;
	private boolean esCuidado;
	
	
	//Constructores
	public Producto(String nombre, double precio, boolean esCuidado) {
	
		this.esCuidado = esCuidado;
		this.nombre = nombre;
		this.precio  = precio;
	}

	public Producto(String nombre, double precio) {
		
		this.esCuidado = false;
		this.nombre = nombre;
		this.precio  = precio;
	}
	//Setters
	
	//Getters
	public double getPrecio() {
		
		return this.precio;
	}

	public String getNombre() {
		
		return this.nombre;
	}

	public boolean esPrecioCuidado() {
		
		return this.esCuidado;
	}	
	
	//Metodos
	
	public void aumentarPrecio(double aumento) {
		
		this.precio = this.precio + aumento;
		
	}

}
