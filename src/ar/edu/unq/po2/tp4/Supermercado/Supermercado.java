package ar.edu.unq.po2.tp4.Supermercado;

import java.util.ArrayList;

public class Supermercado {
	
	// Variables de instancia
	private String nombre;
	private String direccion;
	private ArrayList<Producto> listaDeProductos;

	// Constructor
	public Supermercado(String nombre, String direccion) {
		
		this.nombre  = nombre;
		this.direccion = direccion;
		this.listaDeProductos = new ArrayList<Producto>();
	}

	//Setters
	
	//Getters
	
	public String getNombre() {
		return nombre;
	}

	public String getDireccion() {
		return direccion;
	}
	
	
	//Metodos
	public Integer getCantidadDeProductos() {
		
		return this.listaDeProductos.size();
	}

	

	public void agregarProducto(Producto unProducto) {
		
		this.listaDeProductos.add(unProducto);		
	}
	

	public Double getPrecioTotal() {
		
		double precTot = 0;
		
		for(Producto elem:listaDeProductos) {
			
			precTot += elem.getPrecio();			
		}		
		return precTot;
	}

}








