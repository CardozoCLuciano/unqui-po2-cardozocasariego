package ar.edu.unq.po2.tp4.Trabajador;

import java.util.ArrayList;

public class Trabajador {
	
	//Variables de instancia
	
	private ArrayList<Ingreso> listaDeIngresos;
	
	
	//Constructores
	public Trabajador() {
		
		this.listaDeIngresos = new ArrayList<Ingreso>();		
	}

	
	//Setters
	
	
	//Getters	
	
	
	//Metodos
	
	public Double getTotalPercibido() {
		
		double totalPercibido = 0d;		
	
		for(Ingreso elem:listaDeIngresos){
			totalPercibido += elem.getMonto();
		}	
			
		return totalPercibido;
	}


	public void añadirIngreso(Ingreso unIngreso) {
		
		this.listaDeIngresos.add(unIngreso);
		
	}


	public double getMontoImponible() {
		
		double totalMontoImponible = 0d;
		
		
		for(Ingreso elem:listaDeIngresos){
			
			if(elem.getClass().equals(IngresoHorasExtras.class)) {
				
				totalMontoImponible += 0;
				
			}else {
				
				totalMontoImponible += elem.getMonto();
			}			
		}		
		
		return totalMontoImponible;
	}


	public double getImpuestoAPagar() {
		
		double impuestoTotal = this.getMontoImponible() * 0.02;	
		
		return impuestoTotal;
	}
	

}
