package ar.edu.unq.po2.tp4.Trabajador;

public class IngresoHorasExtras extends Ingreso{

	private int cantHorasExtras;
	
	public IngresoHorasExtras(String unMes, String unDetalle, double unMonto, int horasExtras) {
		
		super(unMes, unDetalle, unMonto);
		this.cantHorasExtras = horasExtras;		
	}
	
	

	public int getCantHorasExtras() {
		return cantHorasExtras;
	}

}
