package ar.edu.unq.po2.tp4.Trabajador;

public class Ingreso {
	
	//Atributos
	private String mes;
	private String concepto;
	private double monto;

	//Constructor
	public Ingreso(String unMes, String unDetalle, double unMonto) {
		
		this.mes = unMes;
		this.concepto = unDetalle;
		this.monto = unMonto;
	}

	//Getter
	public double getMonto() {
		
		return this.monto;
	}

	public String getMes() {
		return mes;
	}

	public String getConcepto() {
		return concepto;
	}
	
	
	



	
	

}
