package ar.edu.unq.po2.tp3;

public class Rectangulo {
	
	// Atributos
	private int largo;
	private int alto;
	private Point puntoSuperiorDerecho;
	private Point puntoSuperiorIzquierdo;
	private Point puntoInferiorDerecho;
	private Point puntoInferiorIzquierdo;
	
	
	//Constructor
	public Rectangulo(Point puntoOrigen, int unLargo, int unAlto) {
		
		this.alto = unAlto;
		this.largo = unLargo;
		
		this.puntoSuperiorIzquierdo = puntoOrigen;
		this.puntoSuperiorDerecho = new Point(puntoOrigen.getCoordenadasX(), (puntoOrigen.getCoordenadasY() + unLargo));	
		this.puntoInferiorIzquierdo = new Point( (puntoOrigen.getCoordenadasX() - unAlto ), puntoOrigen.getCoordenadasY() );
		this.puntoInferiorDerecho = new Point( (puntoOrigen.getCoordenadasX() - unAlto ), (puntoOrigen.getCoordenadasY() + unLargo));
		
	}

	
	//Getters
	public Point getPuntoSuperiorDerecho() {
		return puntoSuperiorDerecho;
	}

	public Point getPuntoSuperiorIzquierdo() {
		return puntoSuperiorIzquierdo;
	}

	public Point getPuntoInferiorDerecho() {
		return puntoInferiorDerecho;
	}

	public Point getPuntoInferiorIzquierdo() {
		return puntoInferiorIzquierdo;
	}
	
	//Metodos
	
	public float calcularArea() {
		
		float area = (this.alto) * (this.largo);
		
		return area;
	}
	
	public float calcularPerimetro() {
		
		float perimetro = 2*(this.alto) + 2*(this.largo);
		
		return perimetro;
	}
	
	
	public String verticalUorizontal() {
		
		if(this.alto > this.largo) {
			
			return "Es vertical";
		}else {
			
			return "Es horizontal";
		}
	}
	
	public void reubicarRectangulo(Point unPunto) {  // Mueve el punto central (superior Izquierdo) y con eso mueve el resto de puntos
		
		this.puntoSuperiorIzquierdo = unPunto;
		this.puntoSuperiorDerecho = new Point(unPunto.getCoordenadasX(), (unPunto.getCoordenadasY() + this.largo));	
		this.puntoInferiorIzquierdo = new Point( (unPunto.getCoordenadasX() - this.alto ), unPunto.getCoordenadasY() );
		this.puntoInferiorDerecho = new Point( (unPunto.getCoordenadasX() - this.alto ), (unPunto.getCoordenadasY() + this.largo));
		
	}
	
	
	
	
	
	
}
