package ar.edu.unq.po2.tp3;

import java.util.ArrayList;

public class MultiOperador {
	
		//Atributos	
		private ArrayList<Integer> listaDeNumeros;
		
		//Constructor
		public MultiOperador(){
			
			listaDeNumeros = new ArrayList<Integer>();
		}
		
		
		//Metodos
		
		public void addNumber(Integer unNumero) {  
			
			listaDeNumeros.add(unNumero);			
		}
		
		
		public int sumarLaLista() {
			
			int resultado = 0;
			
			for(int elem:listaDeNumeros) {
				
				resultado += elem;
				
			}
			return resultado;
		}
		
		public int restarLaLista() {
			
			int resultado = this.listaDeNumeros.get(0);
			
			this.listaDeNumeros.remove(0);
			
			for(int elem:this.listaDeNumeros){				
				
				resultado -= elem;
				
			}
			return resultado;
		}
		
		public int multiplicarLaLista() {
			
			int resultado = this.listaDeNumeros.get(0);			
			this.listaDeNumeros.remove(0);
			
			for(int elem:listaDeNumeros) {
				
				resultado *= elem;
			}
			return resultado;
		}

		
		

	

}
