package ar.edu.unq.po2.tp3;

import java.util.ArrayList;

public class EquipoDeTrabajo {
	//Atributos
	private String nombre;
	private ArrayList<Persona> empleados;
	
	//Constructor
	public EquipoDeTrabajo(String nombre) {
		this.nombre = nombre;
		this.empleados = new ArrayList<Persona>();
	}
		
	
	public String getNombre() {
		
		return this.nombre;
	}
	
	public void addEmpleado(Persona unEmpleado) {
		empleados.add(unEmpleado);
		
	}
	
	public int edadPromedioEmpleados() {
		
		int resultado = 0;
		for(Persona elem:empleados) {
			
			resultado += elem.getEdad(); 		
		}
		resultado = (resultado / empleados.size()) ;
		
		return resultado;
	}

	public static void main(String[] args) {		
		
		
		EquipoDeTrabajo unEquipo = new EquipoDeTrabajo("Luchoss");
		
		Persona pers1 = new Persona("Luis", "Ochoa", 7);
		Persona pers2 = new Persona("Luis", "Ochoa", 10);
		Persona pers3 = new Persona("Luis", "Ochoa", 10);
		Persona pers4 = new Persona("Luis", "Ochoa", 8);
		Persona pers5 = new Persona("Luis", "Ochoa", 2);
		
		unEquipo.addEmpleado(pers1);
		unEquipo.addEmpleado(pers2);
		unEquipo.addEmpleado(pers3);
		unEquipo.addEmpleado(pers4);
		unEquipo.addEmpleado(pers5);
		
		int edadMedia = unEquipo.edadPromedioEmpleados();
		
		System.out.println(edadMedia);
		
		
		
	}

}
