package ar.edu.unq.po2.tp3;

public class Point {
	
	//Atributos
	private int coordX;
	private int coordY;
	
	//Constructor
	public Point() {
		
		this.coordX = 0;
		this.coordY = 0;
	}
	
	public Point(int X, int Y) {
		this.coordX = X;
		this.coordY = Y;
	}

	//Getters
	public int getCoordenadasX() {
		
		return this.coordX;
	}

	public int getCoordenadasY() {
		
		return this.coordY;
	}
	
	//Metodos

	public void moverPuntoA(int X, int Y) {
		
		this.coordX = X;
		this.coordY = Y;		
	}

	public void sumarCoordCon(Point unPunto) {
		
		this.coordX += unPunto.getCoordenadasX(); 
		this.coordY += unPunto.getCoordenadasY();
	}

}




