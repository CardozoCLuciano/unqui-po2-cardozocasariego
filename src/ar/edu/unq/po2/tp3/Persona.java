package ar.edu.unq.po2.tp3;


public class Persona {
	
	//Atributos
	private String nombre;
	private String apellido;
	private int edad; 
	
	// Constructor
	public Persona(String unNombre, String apellido, int unaFecha) {
		
		this.edad = unaFecha;
		this.nombre = unNombre;
		this.apellido = apellido;
		
	}

	// Getters
	public String getNombre() {
		return nombre;
	}
	
	public String getApellido() {
		return apellido;
	}

	public int getEdad() {
		return edad;
	}
	
	//Metodos	
	
	
	public boolean esMenorQue(Persona unaPersona) {
		
		boolean resultado = this.getEdad() < unaPersona.getEdad();
		
		return resultado;
	}
	
	
	
	
	

}
