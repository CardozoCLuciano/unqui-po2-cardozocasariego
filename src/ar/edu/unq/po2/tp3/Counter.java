package ar.edu.unq.po2.tp3;

import java.util.ArrayList;

public class Counter {
	
	//Atributos	
	private ArrayList<Integer> lista;
	
	//Constructor
	public Counter() {
		
		this.lista = new ArrayList<Integer>();
	}
	
	//Metodos
	
	public void addNumber(Integer unNumero) {  
		
		lista.add(unNumero);			
	}
	

	public int cantidadDePares() { 
		
		int cont = 0;		
		for(int unNum:lista){			
			if((unNum % 2) == 0) {				
				cont ++;
			}
		}		
		return cont;
	}
	
	
	public int cantidadDeImares() {
		
		int cont = 0;
		
		for(int unNum:this.lista){
			
			if((unNum % 2) == 1) {
				
				cont ++;
			}
		}
		
		return cont;
	}
	
	
	public int multiplosDe(int unNumero) { 
		
		int cont = 0;
		
		for(int elem:this.lista) {
			
			if((elem % unNumero) == 0) {
				
				cont ++;
			}
		}
		
		return cont;
	}

}
